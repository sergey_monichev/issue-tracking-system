Feature: Issue can be created and selected

	Scenario: I cleanup database and check there is no issues
		Given clean database
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[]
			"""

	Scenario: I create the first issue
		When I call POST method for /v1/issue?name=new-issue1
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue1",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I check there is one issue
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[
				{
					"id": 1,
					"name": "new-issue1",
					"workflow": { "id": 1, "name": "default-workflow" },
					"state": { "id": 2, "name": "open" }
				}	
			]
			"""

	Scenario: I create the second issue
		When I call POST method for /v1/issue?name=new-issue2
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 2,
				"name": "new-issue2",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I check there are two issues
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[
				{
					"id": 1,
					"name": "new-issue1",
					"workflow": { "id": 1, "name": "default-workflow" },
					"state": { "id": 2, "name": "open" }
				},
				{
					"id": 2,
					"name": "new-issue2",
					"workflow": { "id": 1, "name": "default-workflow" },
					"state": { "id": 2, "name": "open" }
				}
			]
			"""

	Scenario: I get issue the first issue by id
		When I call GET method for /v1/issue/1
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue1",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I try to get non existing issue
		When I call GET method for /v1/issue/0
		Then I get status code of 404
