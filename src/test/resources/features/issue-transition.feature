Feature: Transitions can be performed

	Scenario: I cleanup database and check there is no issues
		Given clean database
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[]
			"""

	Scenario: I create the first issue
		When I call POST method for /v1/issue?name=new-issue1
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue1",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I get available transitions list
		When I call GET method for /v1/issue/1/transition
		Then I get status code of 200
		And I get json
			"""
			[
				{ "action": "open -> resolved", "id": 8, "name": "Resolve Issue", "from": { "id": 2, "name": "open" }, "to": { "id": 5, "name": "resolved" } },
	            { "action": "open -> closed", "id": 11, "name": "Close Issue", "from": { "id": 2, "name": "open" }, "to": { "id": 3, "name": "closed" } },
	            { "action": "open -> inProgress", "id": 14, "name": "Start Progress", "from": { "id": 2, "name": "open" }, "to": { "id": 4, "name": "in progress" } }
	        ]
			"""

	Scenario: I perform "Close Issue" transition and get to "closed" state
		When I call POST method for /v1/issue/1/transition/11
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue1",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 3, "name": "closed" }
			}
			"""

	Scenario: I get available transitions list and get actual transitions
		When I call GET method for /v1/issue/1/transition
		Then I get status code of 200
		And I get json
			"""
			[
				{ "action": "closed -> reopen", "id": 17, "name": "Reopen Issue", "from": { "id": 3, "name": "closed" }, "to": { "id": 6, "name": "reopened" } }
		    ]
			"""

	Scenario: I perform "Reopen Issue" transition and get to "reopened" state and see an action
		When I call POST method for /v1/issue/1/transition/17
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue1",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 6, "name": "reopened" }
			}
			"""
		And I see an action performed closed -> reopen

	Scenario: I try to perform "Reopen Issue" transition in wrong state
		When I call POST method for /v1/issue/1/transition/17
		Then I get status code of 409
