Feature: Issue can be created and selected

	Scenario: I cleanup database and check there is no issues
		Given clean database
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[]
			"""

	Scenario: I create the first issue
		When I call POST method for /v1/issue?name=new-issue1
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue1",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I update the first issue
		When I call PUT method for /v1/issue/1?name=New Name
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "New Name",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I try to update non existing issue
		When I call PUT method for /v1/issue/0?name=New Name
		Then I get status code of 404
