Feature: Issue can be deleted

	Scenario: I cleanup database and check there is no issues
		Given clean database
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[]
			"""

	Scenario: I create the first issue
		When I call POST method for /v1/issue?name=new-issue
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I create the second issue
		When I call POST method for /v1/issue?name=new-issue
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 2,
				"name": "new-issue",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I check there are two issues
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[
				{
					"id": 1,
					"name": "new-issue",
					"workflow": { "id": 1, "name": "default-workflow" },
					"state": { "id": 2, "name": "open" }
				},
				{
					"id": 2,
					"name": "new-issue",
					"workflow": { "id": 1, "name": "default-workflow" },
					"state": { "id": 2, "name": "open" }
				}
			]
			"""

	Scenario: I delete the first issue
		When I call DELETE method for /v1/issue/1
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 1,
				"name": "new-issue",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I check there is only the second issue
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[
				{
					"id": 2,
					"name": "new-issue",
					"workflow": { "id": 1, "name": "default-workflow" },
					"state": { "id": 2, "name": "open" }
				}
			]
			"""

	Scenario: I delete the second issue
		When I call DELETE method for /v1/issue/2
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 2,
				"name": "new-issue",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I check there is no issues
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[]
			"""

	Scenario: I try to delete non existing issue
		When I call DELETE method for /v1/issue/2
		Then I get status code of 404
