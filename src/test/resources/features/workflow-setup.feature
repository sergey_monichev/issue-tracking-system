Feature: Workflow can be extensible

	Scenario: I cleanup database and check there is no issues
		Given clean database without resetting sequence
		When I call GET method for /v1/issue
		Then I get status code of 200
		And I get json
			"""
			[]
			"""

	Scenario: I get workflows
		When I call GET method for /v1/workflow
		Then I get status code of 200
		And I get json
			"""
			[
				{ "id": 1, "name": "default-workflow" }
			]
			"""

	Scenario: I get workflow states
		When I call GET method for /v1/workflow/1/state
		Then I get status code of 200
		And I get json
			"""
			[
				{ "id": 2, "name": "open" },
	            { "id": 3, "name": "closed" },
	            { "id": 4, "name": "in progress" },
	            { "id": 5, "name": "resolved" },
	            { "id": 6, "name": "reopened" }
	        ]
			"""

	Scenario: I get workflow states
		When I call GET method for /v1/workflow/1/transition
		Then I get status code of 200
		And I get json
			"""
			[
				{ "action": "0 -> open", "from": null, "id": 7, "name": "Create Issue", "to": { "id": 2, "name": "open" } },
			    { "action": "open -> resolved", "from": { "id": 2, "name": "open" }, "id": 8, "name": "Resolve Issue", "to": { "id": 5, "name": "resolved" } },
			    { "action": "inProgress -> resolved", "from": { "id": 4, "name": "in progress" }, "id": 9, "name": "Resolve Issue", "to": { "id": 5, "name": "resolved" } },
			    { "action": "reopened -> resolved", "from": { "id": 6, "name": "reopened" }, "id": 10, "name": "Resolve Issue", "to": { "id": 5, "name": "resolved" } },
			    { "action": "open -> closed", "from": { "id": 2, "name": "open" }, "id": 11, "name": "Close Issue", "to": { "id": 3, "name": "closed" } },
			    { "action": "reopened -> closed", "from": { "id": 6, "name": "reopened" }, "id": 12, "name": "Close Issue", "to": { "id": 3, "name": "closed" } },
			    { "action": "inProgress -> closed", "from": { "id": 4, "name": "in progress" }, "id": 13, "name": "Close Issue", "to": { "id": 3, "name": "closed" } },
			    { "action": "open -> inProgress", "from": { "id": 2, "name": "open" }, "id": 14, "name": "Start Progress", "to": { "id": 4, "name": "in progress" } },
			    { "action": "reopened -> inProgress", "from": { "id": 6, "name": "reopened" }, "id": 15, "name": "Start Progress", "to": { "id": 4, "name": "in progress" } },
			    { "action": "inProgress -> open", "from": { "id": 4, "name": "in progress" }, "id": 16, "name": "Stop Progress", "to": { "id": 2, "name": "open" } },
			    { "action": "closed -> reopen", "from": { "id": 3, "name": "closed" }, "id": 17, "name": "Reopen Issue", "to": { "id": 6, "name": "reopened" } }
		    ]
			"""

	Scenario: I get workflow initial transition
		When I call GET method for /v1/workflow/1/transition/initial
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 7,
				"name": "Create Issue",
				"action": "0 -> open",
				"from": null,
				"to": { "id": 2, "name": "open" }
			}
			"""

	Scenario: I update workflow initial transition
		When I call PUT method for /v1/workflow/1/transition/initial?name=Test Transition&toStateId=4&action=Test Action
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 7,
				"name": "Test Transition",
				"action": "Test Action",
				"from": null,
				"to": { "id": 4, "name": "in progress" }
			}
			"""

	Scenario: I delete transition
		When I call DELETE method for /v1/workflow/1/transition/13
		Then I get status code of 200
		And I get json
			"""
			{
				"action": "inProgress -> closed",
				"from": { "id": 4, "name": "in progress" },
				"id": 13,
				"name": "Close Issue",
				"to": { "id": 3, "name": "closed" }
			}
			"""

	Scenario: I try to delete non existing transition
		When I call DELETE method for /v1/workflow/1/transition/13
		Then I get status code of 409

	Scenario: I delete state
		When I call DELETE method for /v1/workflow/1/state/6
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 6,
				"name": "reopened"
			}
			"""

	Scenario: I check that associated with state transitions were deleted
		When I call GET method for /v1/workflow/1/transition
		Then I get status code of 200
		And I get json
			"""
			[
				{ "action": "Test Action", "from": null, "id": 7, "name": "Test Transition", "to": { "id": 4, "name": "in progress" } },
			    { "action": "open -> resolved", "from": { "id": 2, "name": "open" }, "id": 8, "name": "Resolve Issue", "to": { "id": 5, "name": "resolved" } },
			    { "action": "inProgress -> resolved", "from": { "id": 4, "name": "in progress" }, "id": 9, "name": "Resolve Issue", "to": { "id": 5, "name": "resolved" } },
			    { "action": "open -> closed", "from": { "id": 2, "name": "open" }, "id": 11, "name": "Close Issue", "to": { "id": 3, "name": "closed" } },
			    { "action": "open -> inProgress", "from": { "id": 2, "name": "open" }, "id": 14, "name": "Start Progress", "to": { "id": 4, "name": "in progress" } },
			    { "action": "inProgress -> open", "from": { "id": 4, "name": "in progress" }, "id": 16, "name": "Stop Progress", "to": { "id": 2, "name": "open" } }
		    ]
			"""

	Scenario: I try to delete non existing state
		When I call DELETE method for /v1/workflow/1/state/6
		Then I get status code of 409

	Scenario: I try to delete state associated with initial transition
		When I call DELETE method for /v1/workflow/1/state/4
		Then I get status code of 409

	Scenario: I create state
		When I call POST method for /v1/workflow/1/state?name=new state
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 18,
				"name": "new state"
			}
			"""

	Scenario: I create transition
		When I call POST method for /v1/workflow/1/transition?name=New Transition&fromStateId=4&toStateId=18&action=New Action
		Then I get status code of 200
		And I get json
			"""
			{
				"action": "New Action",
				"from": { "id": 4, "name": "in progress" },
				"id": 19,
				"name": "New Transition",
				"to": { "id": 18, "name": "new state" }
			}
			"""
		
	Scenario: I check that issue can use new transaction and state
		When I call POST method for /v1/issue?name=New Issue
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 20,
				"name": "New Issue",
				"workflow": { "id": 1, "name": "default-workflow" },
				"state": { "id": 4, "name": "in progress" }
			}
			"""
		When I call GET method for /v1/issue/20/transition
		Then I get status code of 200
		And I get json
			"""
			[
				{ "action": "inProgress -> resolved", "from": { "id": 4, "name": "in progress" }, "id": 9, "name": "Resolve Issue", "to": { "id": 5, "name": "resolved" } },
	            { "action": "inProgress -> open", "from": { "id": 4, "name": "in progress" }, "id": 16, "name": "Stop Progress", "to": { "id": 2, "name": "open" } },
	            { "action": "New Action", "from": { "id": 4, "name": "in progress" }, "id": 19, "name": "New Transition", "to": { "id": 18, "name": "new state" } }
            ]
			"""
		When I call POST method for /v1/issue/20/transition/19
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 20,
				"name": "New Issue",
				"state": { "id": 18, "name": "new state" },
				"workflow": { "id": 1, "name": "default-workflow" }
			}
			"""

	Scenario: I update state
		When I call PUT method for /v1/workflow/1/state/2?name=updated state
		Then I get status code of 200
		And I get json
			"""
			{
				"id": 2,
				"name": "updated state"
			}
			"""

	Scenario: I try to update non existing state
		When I call PUT method for /v1/workflow/1/state/0?name=updated state
		Then I get status code of 409

	Scenario: I update transition
		When I call PUT method for /v1/workflow/1/transition/19?name=updated transition&fromStateId=18&toStateId=4&action=Updated Action
		Then I get status code of 200
		And I get json
			"""
			{
				"action": "Updated Action",
				"from": { "id": 18, "name": "new state" },
				"id": 19,
				"name": "updated transition",
				"to": { "id": 4, "name": "in progress" }
			}
			"""

	Scenario: I try to update non existing state
		When I call PUT method for /v1/workflow/1/state/0?name=updated state
		Then I get status code of 409
