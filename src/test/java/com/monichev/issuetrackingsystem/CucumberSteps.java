package com.monichev.issuetrackingsystem;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.service.RepositoryManagerService;
import com.monichev.issuetrackingsystem.service.impl.SpyTransitionActionServiceImpl;
import io.cucumber.java8.En;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CucumberSteps extends SpringCucumberIntegration implements En {

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private RepositoryManagerService repositoryManagerService;
	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired
	private SpyTransitionActionServiceImpl transitionActionService;

	private ResponseEntity<Object> exchange;

	@PostConstruct
	public void init() {
		When("^I call ([^\"]*) method for ([^\"]*)$", (String method, String path) ->
				exchange = restTemplate.exchange(path, HttpMethod.valueOf(method), null, Object.class));

		Then("^I get status code of (\\d+)$", (Integer expected) ->
				assertThat(exchange.getStatusCodeValue()).isEqualTo(expected));

		And("^I get json$", (String arg) -> {
			String content = arg.replaceAll("\"\"\"", "");
			Object expected;
			if (content.startsWith("[")) {
				expected = new ObjectMapper().readValue(content, List.class);
			} else if (content.startsWith("{")) {
				expected = new ObjectMapper().readValue(content, Map.class);
			} else {
				throw new IllegalArgumentException("Unsupported json content: " + content);
			}
			assertThat(exchange.getBody()).isEqualTo(expected);
		});

		And("^I get empty body$", () ->
				assertThat(exchange.getBody()).isNull());

		Given("^clean database$", () -> {
			repositoryManagerService.cleanup();
			resetHibernateSequence(applicationContext);
			repositoryManagerService.init();
			resetHibernateSequence(applicationContext);
		});

		Given("^clean database without resetting sequence$", () -> {
			repositoryManagerService.cleanup();
			resetHibernateSequence(applicationContext);
			repositoryManagerService.init();
		});

		And("^I see an action performed ([^\"]*)$", (action) -> {
			List<Transition> performedTransactions = transitionActionService.getPerformTransitionActionCalls();
			Transition transition = performedTransactions.get(performedTransactions.size() - 1);
			assertThat(transition.getAction()).isNotEmpty();
			assertThat(transition.getAction()).isEqualTo(action);
		});
	}

	/**
	 * A hack to reset sequence for testing purposes.
	 * Actually we should avoid comparing ids, but for simplification we follow id sequence in tests.
	 *
	 * @param applicationContext application context
	 * @throws SQLException exception
	 */
	private static void resetHibernateSequence(ApplicationContext applicationContext) throws SQLException {
		DataSource dataSource = applicationContext.getBean(DataSource.class);
		try (Connection dbConnection = dataSource.getConnection()) {
			try (Statement statement = dbConnection.createStatement()) {
				statement.execute("ALTER SEQUENCE HIBERNATE_SEQUENCE restart with 1;");
			}
		}
	}

}
