package com.monichev.issuetrackingsystem.service.impl;

import com.monichev.issuetrackingsystem.dto.Transition;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Primary
@Service
public class SpyTransitionActionServiceImpl extends TransitionActionServiceImpl {

	private final List<Transition> performTransitionActionCalls = new ArrayList<>();
	
	public SpyTransitionActionServiceImpl(ThreadPoolTaskExecutor transitionActionExecutor) {
		super(transitionActionExecutor);
	}

	@Override
	public void performTransitionAction(Transition transition) {
		performTransitionActionCalls.add(transition);
		super.performTransitionAction(transition);
	}

	public List<Transition> getPerformTransitionActionCalls() {
		return performTransitionActionCalls;
	}

}
