package com.monichev.issuetrackingsystem.repository;

import com.monichev.issuetrackingsystem.dto.State;
import com.monichev.issuetrackingsystem.dto.Workflow;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;

public interface StateRepository extends CrudRepository<State, Long> {

	Collection<State> findAllByWorkflow(Workflow workflow);

	Optional<State> findByWorkflowAndId(Workflow workflow, Long id);

}
