package com.monichev.issuetrackingsystem.repository;

import com.monichev.issuetrackingsystem.dto.Workflow;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface WorkflowRepository extends CrudRepository<Workflow, Long> {

	@Override
	Collection<Workflow> findAll();

}
