package com.monichev.issuetrackingsystem.repository;

import com.monichev.issuetrackingsystem.dto.Issue;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface IssueRepository extends CrudRepository<Issue, Long> {

	@Override
	Collection<Issue> findAll();

}
