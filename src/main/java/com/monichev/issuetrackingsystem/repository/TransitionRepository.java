package com.monichev.issuetrackingsystem.repository;

import com.monichev.issuetrackingsystem.dto.State;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.dto.Workflow;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;

public interface TransitionRepository extends CrudRepository<Transition, Long> {

	Collection<Transition> findAllByFrom(State from);

	Collection<Transition> findAllByWorkflow(Workflow workflow);

	Optional<Transition> findByWorkflowAndId(Workflow workflow, Long transitionId);

	void deleteAllByFromOrTo(State from, State to);

}
