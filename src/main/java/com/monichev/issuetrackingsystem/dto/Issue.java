package com.monichev.issuetrackingsystem.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Issue {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id = 0;

	@NonNull
	@NotNull
	private String name;

	@NonNull
	@NotNull
	@ManyToOne
	private Workflow workflow;

	@NonNull
	@NotNull
	@ManyToOne
	private State state;

	@SneakyThrows
	@Override
	public String toString() {
		return new ObjectMapper().writeValueAsString(this);
	}

}
