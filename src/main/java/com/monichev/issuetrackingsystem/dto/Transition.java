package com.monichev.issuetrackingsystem.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Table(
		uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "from_id", "workflow_id"})}
)
public class Transition {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id = 0;
	@NonNull
	@NotNull
	private String name;
	@NonNull
	@NotNull
	@ManyToOne
	@JsonIgnore
	private Workflow workflow;
	@ManyToOne
	private State from;
	@NonNull
	@NotNull
	@ManyToOne
	private State to;
	private String action;

	public Transition(String name, Workflow workflow, State from, State to, String action) {
		this.name = name;
		this.workflow = workflow;
		this.from = from;
		this.to = to;
		this.action = action;
	}

	@SneakyThrows
	@Override
	public String toString() {
		return new ObjectMapper().writeValueAsString(this);
	}

}
