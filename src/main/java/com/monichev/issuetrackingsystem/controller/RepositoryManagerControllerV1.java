package com.monichev.issuetrackingsystem.controller;

import com.monichev.issuetrackingsystem.service.RepositoryManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(RepositoryManagerControllerV1.URL)
@Api(description = "Repository manager controller")
public class RepositoryManagerControllerV1 {

	public static final String URL = "/v1/repositoryManager";

	private final RepositoryManagerService repositoryManagerService;

	public RepositoryManagerControllerV1(RepositoryManagerService repositoryManagerService) {
		this.repositoryManagerService = repositoryManagerService;
	}

	@PostMapping("/init")
	@ApiOperation("Fill repository with initial data")
	public void init() {
		repositoryManagerService.cleanup();
		repositoryManagerService.init();
	}

	@PostMapping("/cleanup")
	@ApiOperation("Cleanup repository data")
	public void cleanup() {
		repositoryManagerService.cleanup();
	}

}
