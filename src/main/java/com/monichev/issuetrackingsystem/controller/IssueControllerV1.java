package com.monichev.issuetrackingsystem.controller;

import com.monichev.issuetrackingsystem.dto.Issue;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.service.IssueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(IssueControllerV1.URL)
@Slf4j
@Api(description = "Issue controller")
public class IssueControllerV1 {

	public static final String URL = "/v1/issue";

	private final IssueService issueService;

	public IssueControllerV1(IssueService issueService) {
		this.issueService = issueService;
	}
	
	@GetMapping("/{issueId}")
	@ApiOperation("Get Issue")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Issue not found")
	})
	public Issue getIssue(@PathVariable Long issueId) throws IssueService.IssueNotFoundException {
		return issueService.getIssue(issueId);
	}

	@GetMapping
	@ApiOperation("Get all Issues")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK")
	})
	public Collection<Issue> getIssues() {
		return issueService.getIssues();
	}

	@PostMapping
	@ApiOperation(
			value = "Create a new Issue",
			notes = "Issue will be associated with default workflow. Default workflow should exist before issue creation."
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 500, message = "Default workflow does not exist")
	})
	public Issue createIssue(@RequestParam String name) {
		return issueService.createIssue(name);
	}

	@PutMapping("/{issueId}")
	@ApiOperation("Update Issue")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Issue not found")
	})
	public Issue deleteIssue(@PathVariable Long issueId, @RequestParam String name) throws IssueService.IssueNotFoundException {
		return issueService.updateIssue(issueId, name);
	}

	@DeleteMapping("/{issueId}")
	@ApiOperation("Delete Issue")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Issue not found")
	})
	public Issue deleteIssue(@PathVariable Long issueId) throws IssueService.IssueNotFoundException {
		return issueService.deleteIssue(issueId);
	}

	@GetMapping("/{issueId}/transition")
	@ApiOperation(
			value = "Get Issue Transitions",
			notes = "Return only allowed Transitions according to current State."
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Issue not found")
	})
	public Collection<Transition> getTransitions(@PathVariable Long issueId) throws IssueService.IssueNotFoundException {
		return issueService.getPossibleTransitions(issueId);
	}

	@PostMapping("/{issueId}/transition/{transitionId}")
	@ApiOperation("Do Issue Transition")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Issue not found"),
			@ApiResponse(code = 409, message = "Transition not allowed")
	})
	public Issue doTransition(
			@PathVariable Long issueId,
			@PathVariable Long transitionId
	) throws IssueService.IssueNotFoundException, IssueService.TransitionNotAllowedException {
		return issueService.doTransition(issueId, transitionId);
	}

	@ExceptionHandler(IssueService.IssueNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Issue not found")
	public void handleIssueNotFoundException(Exception exception) {
		log.warn("handleIssueNotFoundException", exception);
	}

	@ExceptionHandler(IssueService.TransitionNotAllowedException.class)
	@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Transition not allowed")
	public void handleTransitionNotAllowedException(Exception exception) {
		log.warn("handleTransitionNotAllowedException", exception);
	}

}
