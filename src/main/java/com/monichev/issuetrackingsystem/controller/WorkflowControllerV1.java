package com.monichev.issuetrackingsystem.controller;

import com.monichev.issuetrackingsystem.dto.State;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.dto.Workflow;
import com.monichev.issuetrackingsystem.service.WorkflowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(WorkflowControllerV1.URL)
@Slf4j
@Api(description = "Workflow controller")
public class WorkflowControllerV1 {

	public static final String URL = "/v1/workflow";

	private final WorkflowService workflowService;

	public WorkflowControllerV1(WorkflowService workflowService) {
		this.workflowService = workflowService;
	}

	@GetMapping("/{workflowId}")
	@ApiOperation("Get Workflow")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Workflow not found")
	})
	public Workflow getWorkflow(@PathVariable Long workflowId) throws WorkflowService.WorkflowNotFoundException {
		return workflowService.getWorkflow(workflowId);
	}

	@GetMapping
	@ApiOperation("Get all Workflows")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK")
	})
	public Collection<Workflow> getWorkflows() {
		return workflowService.getWorkflows();
	}

	@GetMapping("/{workflowId}/state")
	@ApiOperation("Get all Workflow States")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Workflow not found")
	})
	public Collection<State> getWorkflowStates(@PathVariable Long workflowId) throws WorkflowService.WorkflowNotFoundException {
		return workflowService.getWorkflowStates(workflowId);
	}

	@PostMapping("/{workflowId}/state")
	@ApiOperation("Create Workflow State")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Workflow not found")
	})
	public State updateWorkflowState(
			@PathVariable Long workflowId,
			@RequestParam String name
	) throws WorkflowService.WorkflowNotFoundException {
		return workflowService.createWorkflowState(workflowId, name);
	}

	@PutMapping("/{workflowId}/state/{stateId}")
	@ApiOperation("Update Workflow State")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Workflow not found"),
			@ApiResponse(code = 409, message = "State not found")
	})
	public State updateWorkflowState(
			@PathVariable Long workflowId,
			@PathVariable Long stateId,
			@RequestParam String name
	) throws WorkflowService.WorkflowNotFoundException, WorkflowService.StateNotFoundException {
		return workflowService.updateWorkflowState(workflowId, stateId, name);
	}

	@DeleteMapping("/{workflowId}/state/{stateId}")
	@ApiOperation("Delete Workflow State")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Workflow not found"),
			@ApiResponse(code = 409, message = "State not found")
	})
	public State deleteWorkflowState(
			@PathVariable Long workflowId,
			@PathVariable Long stateId
	) throws WorkflowService.WorkflowNotFoundException, WorkflowService.StateNotFoundException {
		return workflowService.deleteWorkflowState(workflowId, stateId);
	}

	@GetMapping("/{workflowId}/transition")
	@ApiOperation("Get all Workflow Transitions")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Workflow not found")
	})
	public Collection<Transition> getWorkflowTransitions(@PathVariable Long workflowId) throws WorkflowService.WorkflowNotFoundException {
		return workflowService.getWorkflowTransitions(workflowId);
	}

	@GetMapping("/{workflowId}/transition/initial")
	@ApiOperation("Get Workflow initial Transition")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Workflow not found")
	})
	public Transition getWorkflowInitialTransition(@PathVariable Long workflowId) throws WorkflowService.WorkflowNotFoundException {
		return workflowService.getWorkflowInitialTransition(workflowId);
	}

	@PutMapping("/{workflowId}/transition/initial")
	@ApiOperation("Update Workflow initial Transition")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Workflow not found")
	})
	public Transition updateWorkflowInitialTransition(
			@PathVariable Long workflowId,
			@RequestParam String name,
			@RequestParam Long toStateId,
			@RequestParam(required = false) String action
	) throws WorkflowService.WorkflowNotFoundException, WorkflowService.StateNotFoundException {
		return workflowService.updateWorkflowInitialTransition(workflowId, name, toStateId, action);
	}

	@PostMapping("/{workflowId}/transition")
	@ApiOperation("Create Workflow Transition")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Workflow not found"),
			@ApiResponse(code = 409, message = "State not found")
	})
	public Transition createWorkflowTransition(
			@PathVariable Long workflowId,
			@RequestParam String name,
			@RequestParam Long fromStateId,
			@RequestParam Long toStateId,
			@RequestParam(required = false) String action
	) throws WorkflowService.WorkflowNotFoundException, WorkflowService.StateNotFoundException {
		return workflowService.createWorkflowTransition(workflowId, name, fromStateId, toStateId, action);
	}

	@PutMapping("/{workflowId}/transition/{transitionId}")
	@ApiOperation("Update Workflow Transition")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Workflow not found"),
			@ApiResponse(code = 409, message = "State or Transition not found"),
	})
	public Transition updateWorkflowTransition(
			@PathVariable Long workflowId,
			@PathVariable Long transitionId,
			@RequestParam String name,
			@RequestParam Long fromStateId,
			@RequestParam Long toStateId,
			@RequestParam(required = false) String action
	) throws WorkflowService.WorkflowNotFoundException, WorkflowService.TransitionNotFoundException, WorkflowService.StateNotFoundException {
		return workflowService.updateWorkflowTransition(workflowId, transitionId, name, fromStateId, toStateId, action);
	}

	@DeleteMapping("/{workflowId}/transition/{transitionId}")
	@ApiOperation("Delete Workflow Transition")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Wrong arguments"),
			@ApiResponse(code = 404, message = "Workflow not found"),
			@ApiResponse(code = 409, message = "Transition not found")
	})
	public Transition deleteWorkflowTransition(
			@PathVariable Long workflowId,
			@PathVariable Long transitionId
	) throws WorkflowService.WorkflowNotFoundException, WorkflowService.TransitionNotFoundException {
		return workflowService.deleteWorkflowTransition(workflowId, transitionId);
	}

	@ExceptionHandler(WorkflowService.WorkflowNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Workflow not found")
	public void handleWorkflowNotFoundException(Exception exception) {
		log.warn("handleWorkflowNotFoundException", exception);
	}

	@ExceptionHandler(WorkflowService.StateNotFoundException.class)
	@ResponseStatus(value = HttpStatus.CONFLICT, reason = "State not found")
	public void handleStateNotFoundException(Exception exception) {
		log.warn("handleStateNotFoundException", exception);
	}

	@ExceptionHandler(WorkflowService.TransitionNotFoundException.class)
	@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Transition not found")
	public void handleTransitionNotFoundException(Exception exception) {
		log.warn("handleTransitionNotFoundException", exception);
	}

}
