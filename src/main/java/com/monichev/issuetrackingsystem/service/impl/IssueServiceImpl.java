package com.monichev.issuetrackingsystem.service.impl;

import com.monichev.issuetrackingsystem.dto.Issue;
import com.monichev.issuetrackingsystem.dto.State;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.dto.Workflow;
import com.monichev.issuetrackingsystem.repository.IssueRepository;
import com.monichev.issuetrackingsystem.repository.TransitionRepository;
import com.monichev.issuetrackingsystem.repository.WorkflowRepository;
import com.monichev.issuetrackingsystem.service.IssueService;
import com.monichev.issuetrackingsystem.service.TransitionActionService;
import lombok.NonNull;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Collection;

@Service
public class IssueServiceImpl implements IssueService {

	private final IssueRepository issueRepository;
	private final WorkflowRepository workflowRepository;
	private final TransitionRepository transitionRepository;
	private final TransitionActionService transitionActionService;

	public IssueServiceImpl(
			IssueRepository issueRepository,
			WorkflowRepository workflowRepository,
			TransitionRepository transitionRepository,
			TransitionActionService transitionActionService
	) {
		this.issueRepository = issueRepository;
		this.workflowRepository = workflowRepository;
		this.transitionRepository = transitionRepository;
		this.transitionActionService = transitionActionService;
	}

	@Override
	public Issue createIssue(String name) {
		Workflow workflow = workflowRepository.findAll().stream()
				.findAny()
				.orElseThrow(() -> new IllegalStateException("Can't find any workflow"));
		Transition initialTransition = workflow.getInitialTransition();
		Issue save = issueRepository.save(new Issue(name, workflow, initialTransition.getTo()));
		performTransitionAction(initialTransition);
		return save;
	}

	@Override
	public Issue updateIssue(Long issueId, String name) throws IssueNotFoundException {
		Issue issue = getIssue(issueId);
		issue.setName(name);
		return issueRepository.save(issue);
	}

	private void performTransitionAction(Transition transition) {
		transitionActionService.performTransitionAction(transition);
	}

	@Override
	public Collection<Issue> getIssues() {
		return issueRepository.findAll();
	}

	@Override
	public Issue deleteIssue(Long issueId) throws IssueNotFoundException {
		Issue issue = getIssue(issueId);
		try {
			issueRepository.deleteById(issueId);
		} catch (EmptyResultDataAccessException e) {
			throw new IssueNotFoundException(issueId, e);
		}
		return issue;
	}

	@Override
	public Issue getIssue(Long issueId) throws IssueNotFoundException {
		return findIssueById(issueId);
	}

	@Override
	public Collection<Transition> getPossibleTransitions(Long issueId) throws IssueNotFoundException {
		Issue issue = findIssueById(issueId);
		@NonNull @NotNull State state = issue.getState();
		return transitionRepository.findAllByFrom(state);
	}

	@Override
	public Issue doTransition(Long issueId, Long transitionId) throws IssueNotFoundException, TransitionNotAllowedException {
		Issue issue = findIssueById(issueId);
		@NonNull @NotNull State state = issue.getState();
		Collection<Transition> transitions = transitionRepository.findAllByFrom(state);
		Transition transition = transitions.stream()
				.filter(it -> transitionId.equals(it.getId()))
				.findAny()
				.orElseThrow(() -> new TransitionNotAllowedException(transitionId));
		@NonNull @NotNull State to = transition.getTo();
		issue.setState(to);
		Issue result = issueRepository.save(issue);
		performTransitionAction(transition);
		return result;
	}

	private Issue findIssueById(Long issueId) throws IssueNotFoundException {
		return issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
	}

}
