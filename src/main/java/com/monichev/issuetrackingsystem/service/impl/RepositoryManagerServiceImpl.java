package com.monichev.issuetrackingsystem.service.impl;

import com.monichev.issuetrackingsystem.dto.State;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.dto.Workflow;
import com.monichev.issuetrackingsystem.repository.IssueRepository;
import com.monichev.issuetrackingsystem.repository.StateRepository;
import com.monichev.issuetrackingsystem.repository.TransitionRepository;
import com.monichev.issuetrackingsystem.repository.WorkflowRepository;
import com.monichev.issuetrackingsystem.service.RepositoryManagerService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RepositoryManagerServiceImpl implements RepositoryManagerService {

	private final IssueRepository issueRepository;
	private final WorkflowRepository workflowRepository;
	private final StateRepository stateRepository;
	private final TransitionRepository transitionRepository;

	public RepositoryManagerServiceImpl(
			IssueRepository issueRepository,
			WorkflowRepository workflowRepository,
			StateRepository stateRepository,
			TransitionRepository transitionRepository) {
		this.issueRepository = issueRepository;
		this.workflowRepository = workflowRepository;
		this.stateRepository = stateRepository;
		this.transitionRepository = transitionRepository;
	}

	@Transactional
	@Override
	public void init() {
		Workflow workflow = workflowRepository.save(new Workflow("default-workflow"));
		State open = stateRepository.save(new State("open", workflow));
		State closed = stateRepository.save(new State("closed", workflow));
		State inProgress = stateRepository.save(new State("in progress", workflow));
		State resolved = stateRepository.save(new State("resolved", workflow));
		State reopened = stateRepository.save(new State("reopened", workflow));

		Transition init = transitionRepository.save(new Transition("Create Issue", workflow, null, open, "0 -> open"));
		transitionRepository.save(new Transition("Resolve Issue", workflow, open, resolved, "open -> resolved"));
		transitionRepository.save(new Transition("Resolve Issue", workflow, inProgress, resolved, "inProgress -> resolved"));
		transitionRepository.save(new Transition("Resolve Issue", workflow, reopened, resolved, "reopened -> resolved"));
		transitionRepository.save(new Transition("Close Issue", workflow, open, closed, "open -> closed"));
		transitionRepository.save(new Transition("Close Issue", workflow, reopened, closed, "reopened -> closed"));
		transitionRepository.save(new Transition("Close Issue", workflow, inProgress, closed, "inProgress -> closed"));
		transitionRepository.save(new Transition("Start Progress", workflow, open, inProgress, "open -> inProgress"));
		transitionRepository.save(new Transition("Start Progress", workflow, reopened, inProgress, "reopened -> inProgress"));
		transitionRepository.save(new Transition("Stop Progress", workflow, inProgress, open, "inProgress -> open"));
		transitionRepository.save(new Transition("Reopen Issue", workflow, closed, reopened, "closed -> reopen"));

		workflow.setInitialTransition(init);

		workflowRepository.save(workflow);
	}

	@Transactional
	@Override
	public void cleanup() {
		transitionRepository.deleteAll();
		issueRepository.deleteAll();
		stateRepository.deleteAll();
		workflowRepository.deleteAll();
	}
}
