package com.monichev.issuetrackingsystem.service.impl;

import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.service.TransitionActionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Slf4j
@Service
public class TransitionActionServiceImpl implements TransitionActionService {

	private final ThreadPoolTaskExecutor transitionActionExecutor;

	public TransitionActionServiceImpl(ThreadPoolTaskExecutor transitionActionExecutor) {
		this.transitionActionExecutor = transitionActionExecutor;
	}

	@Override
	public void performTransitionAction(Transition transition) {
		String action = transition.getAction();
		if (StringUtils.isEmpty(action)) {
			return;
		}
		transitionActionExecutor.execute(() -> log.info("perform action: " + action));
	}

}
