package com.monichev.issuetrackingsystem.service.impl;

import com.monichev.issuetrackingsystem.dto.State;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.dto.Workflow;
import com.monichev.issuetrackingsystem.repository.StateRepository;
import com.monichev.issuetrackingsystem.repository.TransitionRepository;
import com.monichev.issuetrackingsystem.repository.WorkflowRepository;
import com.monichev.issuetrackingsystem.service.WorkflowService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class WorkflowServiceImpl implements WorkflowService {

	private final WorkflowRepository workflowRepository;
	private final TransitionRepository transitionRepository;
	private final StateRepository stateRepository;

	public WorkflowServiceImpl(
			WorkflowRepository workflowRepository,
			TransitionRepository transitionRepository,
			StateRepository stateRepository
	) {
		this.workflowRepository = workflowRepository;
		this.transitionRepository = transitionRepository;
		this.stateRepository = stateRepository;
	}

	@Override
	public Workflow getWorkflow(Long workflowId) throws WorkflowNotFoundException {
		return findWorkflowById(workflowId);
	}

	@Override
	public Collection<Workflow> getWorkflows() {
		return workflowRepository.findAll();
	}

	@Override
	public Collection<State> getWorkflowStates(Long workflowId) throws WorkflowNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		return stateRepository.findAllByWorkflow(workflow);
	}

	@Override
	public Collection<Transition> getWorkflowTransitions(Long workflowId) throws WorkflowNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		return transitionRepository.findAllByWorkflow(workflow);
	}

	@Override
	public State createWorkflowState(Long workflowId, String name) throws WorkflowNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		return stateRepository.save(new State(name, workflow));
	}

	@Override
	public State updateWorkflowState(
			Long workflowId,
			Long stateId,
			String name
	) throws WorkflowNotFoundException, StateNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		State state = findStateByWorkflowAndId(workflow, stateId);
		state.setName(name);
		return stateRepository.save(state);
	}

	@Override
	@Transactional
	public Transition updateWorkflowInitialTransition(
			Long workflowId,
			String name,
			Long toStateId,
			String action
	) throws WorkflowNotFoundException, StateNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		State state = findStateByWorkflowAndId(workflow, toStateId);
		Transition initialTransition = workflow.getInitialTransition();
		initialTransition.setName(name);
		initialTransition.setTo(state);
		initialTransition.setAction(action);
		return transitionRepository.save(initialTransition);
	}

	@Override
	@Transactional
	public Transition createWorkflowTransition(
			Long workflowId,
			String name,
			Long fromStateId,
			Long toStateId,
			String action
	) throws WorkflowNotFoundException, StateNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		State fromState = findStateByWorkflowAndId(workflow, fromStateId);
		State toState = findStateByWorkflowAndId(workflow, toStateId);
		return transitionRepository.save(new Transition(name, workflow, fromState, toState, action));
	}

	@Override
	public Transition updateWorkflowTransition(
			Long workflowId,
			Long transitionId,
			String name,
			Long fromStateId,
			Long toStateId,
			String action
	) throws WorkflowNotFoundException, TransitionNotFoundException, StateNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		Transition transition = findTransitionByWorkflowAndId(workflow, transitionId);
		if (transition.getFrom() == null) {
			// protect initial transitions from update in this method, as it should be updateWorkflowInitialTransition
			throw new TransitionNotFoundException(transitionId);
		}
		State fromState = findStateByWorkflowAndId(workflow, fromStateId);
		State toState = findStateByWorkflowAndId(workflow, toStateId);
		transition.setName(name);
		transition.setFrom(fromState);
		transition.setTo(toState);
		transition.setAction(action);
		return transitionRepository.save(transition);
	}

	@Override
	public Transition deleteWorkflowTransition(
			Long workflowId,
			Long transitionId
	) throws WorkflowNotFoundException, TransitionNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		Transition transition = findTransitionByWorkflowAndId(workflow, transitionId);
		if (transition.getFrom() == null) {
			// protect initial transitions from deletion
			throw new TransitionNotFoundException(transitionId);
		}
		transitionRepository.delete(transition);
		return transition;
	}

	@Override
	@Transactional
	public State deleteWorkflowState(Long workflowId, Long stateId) throws WorkflowNotFoundException, StateNotFoundException {
		Workflow workflow = findWorkflowById(workflowId);
		if (stateId == workflow.getInitialTransition().getTo().getId()) {
			// protect initial transition state from deletion
			throw new StateNotFoundException(stateId);
		}
		State state = findStateByWorkflowAndId(workflow, stateId);
		transitionRepository.deleteAllByFromOrTo(state, state);
		stateRepository.delete(state);
		return state;
	}

	private Workflow findWorkflowById(Long workflowId) throws WorkflowNotFoundException {
		return workflowRepository.findById(workflowId).orElseThrow(() -> new WorkflowNotFoundException(workflowId));
	}

	private State findStateByWorkflowAndId(Workflow workflow, Long stateId) throws StateNotFoundException {
		return stateRepository.findByWorkflowAndId(workflow, stateId).orElseThrow(() -> new StateNotFoundException(stateId));
	}

	private Transition findTransitionByWorkflowAndId(Workflow workflow, Long transitionId) throws TransitionNotFoundException {
		return transitionRepository.findByWorkflowAndId(workflow, transitionId).orElseThrow(() -> new TransitionNotFoundException(transitionId));
	}

	@Override
	public Transition getWorkflowInitialTransition(Long workflowId) throws WorkflowNotFoundException {
		Workflow workflowById = findWorkflowById(workflowId);
		return workflowById.getInitialTransition();
	}

}
