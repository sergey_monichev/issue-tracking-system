package com.monichev.issuetrackingsystem.service;

import com.monichev.issuetrackingsystem.dto.State;
import com.monichev.issuetrackingsystem.dto.Transition;
import com.monichev.issuetrackingsystem.dto.Workflow;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Collection;

public interface WorkflowService {

	/**
	 * Get a collection of all {@code Workflow}s.
	 *
	 * @return collection of all {@code Workflow}s
	 */
	@NonNull
	Collection<Workflow> getWorkflows();

	/**
	 * Get a {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId {@code Workflow} id
	 * @return {@code Workflow}
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 */
	@NonNull
	Workflow getWorkflow(@NonNull Long workflowId) throws WorkflowNotFoundException;

	/**
	 * Get a collection of all {@code State}s of the {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId {@code Workflow} id
	 * @return collection of {@code State}s
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 */
	@NonNull
	Collection<State> getWorkflowStates(@NonNull Long workflowId) throws WorkflowNotFoundException;

	/**
	 * Get a collection of all {@code Transition}s of the {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId {@code Workflow} id
	 * @return collection of {@code Transition}s
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 */
	@NonNull
	Collection<Transition> getWorkflowTransitions(@NonNull Long workflowId) throws WorkflowNotFoundException;

	/**
	 * Create a {@code State} with given {@code name} for the {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId {@code Workflow} id
	 * @param name       {@code State} name
	 * @return created {@code State}
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 */
	@NonNull
	State createWorkflowState(@NonNull Long workflowId, @NonNull String name) throws WorkflowNotFoundException;

	/**
	 * Update a {@code State} with id {@code stateId} of the {@code Workflow} with id {@code workflowId}
	 * with given {@code name}
	 *
	 * @param workflowId {@code Workflow} id
	 * @param stateId    {@code State} id
	 * @param name       {@code Transition} name
	 * @return updated {@code State}
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 * @throws StateNotFoundException    {@code State} with id {@code stateId} was not found
	 */
	@NonNull
	State updateWorkflowState(
			@NonNull Long workflowId,
			@NonNull Long stateId,
			@NonNull String name
	) throws WorkflowNotFoundException, StateNotFoundException;

	/**
	 * Create a {@code Transition} with given {@code name}, {@code fromStateId}, {@code toStateId} and {@code action}
	 * for the {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId  {@code Workflow} id
	 * @param name        {@code Transition} name
	 * @param fromStateId {@code Transition} from {@code State} id
	 * @param toStateId   {@code Transition} to {@code State} id
	 * @param action      {@code Transition} action
	 * @return created {@code Transition}
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 * @throws StateNotFoundException    {@code State} with id {@code fromStateId} or {@code toStateId} was not found
	 */
	@NonNull
	Transition createWorkflowTransition(
			@NonNull Long workflowId,
			@NonNull String name,
			@NonNull Long fromStateId,
			@NonNull Long toStateId,
			@Nullable String action
	) throws WorkflowNotFoundException, StateNotFoundException;

	/**
	 * Update a {@code Transition} with id {@code transitionId} of the {@code Workflow} with id {@code workflowId}
	 * with given {@code name}, {@code fromStateId}, {@code toStateId} and {@code action}
	 * <p>
	 * Initial {@code Transition}s should be updated using
	 * {@link WorkflowService#updateWorkflowInitialTransition(Long, String, Long, String)} method.
	 *
	 * @param workflowId   {@code Workflow} id
	 * @param transitionId {@code Transition} id
	 * @param name         {@code Transition} name
	 * @param fromStateId  {@code Transition} from {@code State} id
	 * @param toStateId    {@code Transition} to {@code State} id
	 * @param action       {@code Transition} action
	 * @return updated {@code Transition}
	 * @throws WorkflowNotFoundException   {@code Workflow} with id {@code workflowId} was not found
	 * @throws TransitionNotFoundException {@code Transition} with id {@code transitionId} was not found
	 * @throws StateNotFoundException      {@code State} with id {@code fromStateId} or {@code toStateId} was not found
	 * @see WorkflowService#updateWorkflowInitialTransition(Long, String, Long, String)
	 */
	@NonNull
	Transition updateWorkflowTransition(
			@NonNull Long workflowId,
			@NonNull Long transitionId,
			@NonNull String name,
			@NonNull Long fromStateId,
			@NonNull Long toStateId,
			@Nullable String action
	) throws WorkflowNotFoundException, TransitionNotFoundException, StateNotFoundException;

	/**
	 * Update initial {@code Transition} with given {@code name}, {@code fromStateId}, {@code toStateId} and {@code action}
	 * for the {@code Workflow} with id {@code workflowId}.
	 * <p>
	 * Initial {@code Transition} is unique over {@code Workflow} and has {@link Transition#getFrom()} equals {@code null}. 
	 *
	 * @param workflowId {@code Workflow} id
	 * @param name       {@code Transition} name
	 * @param toStateId  {@code Transition} to {@code State} id
	 * @param action     {@code Transition} action
	 * @return updated {@code Transition}
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 * @throws StateNotFoundException    {@code State} with {@code toStateId} was not found
	 */
	@NonNull
	Transition updateWorkflowInitialTransition(
			@NonNull Long workflowId,
			@NonNull String name,
			@NonNull Long toStateId,
			@Nullable String action
	) throws WorkflowNotFoundException, StateNotFoundException;

	/**
	 * Delete a {@code Transition} with id {@code transitionId}
	 * for the {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId   {@code Workflow} id
	 * @param transitionId {@code Transition} id
	 * @return deleted {@code Transition}
	 * @throws WorkflowNotFoundException   {@code Workflow} with id {@code workflowId} was not found
	 * @throws TransitionNotFoundException {@code Transition} with id {@code transitionId} was not found
	 */
	@NonNull
	Transition deleteWorkflowTransition(@NonNull Long workflowId, @NonNull Long transitionId) throws WorkflowNotFoundException, TransitionNotFoundException;

	/**
	 * Delete a {@code State} with id {@code stateId}
	 * for the {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId {@code Workflow} id
	 * @param stateId    {@code State} id
	 * @return deleted {@code State}
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 * @throws StateNotFoundException    {@code State} with id {@code stateId} was not found
	 */
	@NonNull
	State deleteWorkflowState(@NonNull Long workflowId, @NonNull Long stateId) throws WorkflowNotFoundException, StateNotFoundException;

	/**
	 * Get an initial {@code Transition} of the {@code Workflow} with id {@code workflowId}.
	 *
	 * @param workflowId {@code Workflow} id
	 * @return {@code Transition}
	 * @throws WorkflowNotFoundException {@code Workflow} with id {@code workflowId} was not found
	 */
	@NonNull
	Transition getWorkflowInitialTransition(@NonNull Long workflowId) throws WorkflowNotFoundException;

	/**
	 * Thrown to indicate that {@code Workflow} with id {@code workflowId} was not found.
	 */
	class WorkflowNotFoundException extends Exception {

		public WorkflowNotFoundException(@NonNull Long workflowId) {
			this(workflowId, null);
		}

		public WorkflowNotFoundException(@NonNull Long workflowId, @Nullable Throwable cause) {
			super("Workflow with id " + workflowId + " not found!", cause);
		}

	}

	/**
	 * Thrown to indicate that {@code State} with id {@code stateId} was not found.
	 */
	class StateNotFoundException extends Exception {

		public StateNotFoundException(@NonNull Long stateId) {
			this(stateId, null);
		}

		public StateNotFoundException(@NonNull Long stateId, @Nullable Throwable cause) {
			super("State with id " + stateId + " not found!", cause);
		}

	}

	/**
	 * Thrown to indicate that {@code Transition} with id {@code transitionId} was not found.
	 */
	class TransitionNotFoundException extends Exception {

		public TransitionNotFoundException(@NonNull Long transitionId) {
			this(transitionId, null);
		}

		public TransitionNotFoundException(@NonNull Long transitionId, @Nullable Throwable cause) {
			super("Transition with id " + transitionId + " not found!", cause);
		}

	}

}
