package com.monichev.issuetrackingsystem.service;

import com.monichev.issuetrackingsystem.dto.Transition;
import org.springframework.lang.NonNull;

/**
 * A service interface to perform {@code Transition} actions.
 */
public interface TransitionActionService {

	/**
	 * Perform {@code Transition} action.
	 *
	 * @param transition {@code Transition}
	 */
	void performTransitionAction(@NonNull Transition transition);

}
