package com.monichev.issuetrackingsystem.service;

import com.monichev.issuetrackingsystem.dto.Issue;
import com.monichev.issuetrackingsystem.dto.Transition;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Collection;

/**
 * A service interface to operate {@code Issue}s' lifecycle.
 */
public interface IssueService {

	/**
	 * Create an {@code Issue} with given {@code name}.
	 *
	 * @param name {@code Issue} name
	 * @return created {@code Issue}
	 */
	@NonNull
	Issue createIssue(@NonNull String name);

	/**
	 * Get a collection of all {@code Issue}s.
	 *
	 * @return collection of all {@code Issue}s
	 */
	@NonNull
	Collection<Issue> getIssues();

	/**
	 * Get an {@code Issue} with id {@code issueId}.
	 *
	 * @param issueId {@code Issue} id
	 * @return {@code Issue}
	 * @throws IssueNotFoundException {@code Issue} with id {@code issueId} was not found
	 */
	@NonNull
	Issue getIssue(@NonNull Long issueId) throws IssueNotFoundException;

	/**
	 * Update an {@code Issue} with id {@code issueId}.
	 *
	 * @param issueId {@code Issue} id
	 * @param name    {@code Issue} name
	 * @return updated {@code Issue}
	 * @throws IssueNotFoundException {@code Issue} with id {@code issueId} was not found
	 */
	@NonNull
	Issue updateIssue(@NonNull Long issueId, @NonNull String name) throws IssueNotFoundException;

	/**
	 * Delete an {@code Issue} with id {@code issueId}.
	 *
	 * @param issueId {@code Issue} id
	 * @return deleted {@code Issue}
	 * @throws IssueNotFoundException {@code Issue} with id {@code issueId} was not found
	 */
	@NonNull
	Issue deleteIssue(@NonNull Long issueId) throws IssueNotFoundException;

	/**
	 * Get a collection of possible {@code Transition}s of the {@code Issue} with id {@code issueId}.
	 *
	 * @return collection of {@code Transition}s
	 */
	@NonNull
	Collection<Transition> getPossibleTransitions(@NonNull Long issueId) throws IssueNotFoundException;

	/**
	 * Do {@code Transition} for the {@code Issue} with id {@code issueId}.
	 *
	 * @param issueId      {@code Issue} id
	 * @param transitionId {@code Transition} id
	 * @return {@code Issue} after {@code Transition} done
	 * @throws IssueNotFoundException        {@code Issue} with id {@code issueId} was not found
	 * @throws TransitionNotAllowedException {@code Transition} with id {@code transitionId} is not possible for current issue
	 */
	@NonNull
	Issue doTransition(@NonNull Long issueId, @NonNull Long transitionId) throws IssueNotFoundException, TransitionNotAllowedException;

	/**
	 * Thrown to indicate that {@code Issue} with id {@code issueId} was not found.
	 */
	class IssueNotFoundException extends Exception {

		public IssueNotFoundException(@NonNull Long issueId) {
			this(issueId, null);
		}

		public IssueNotFoundException(@NonNull Long issueId, @Nullable Throwable cause) {
			super("Issue with id " + issueId + " not found!", cause);
		}

	}

	/**
	 * Thrown to indicate that {@code Transition} with id {@code transitionId} was not found.
	 */
	class TransitionNotAllowedException extends Exception {

		public TransitionNotAllowedException(@NonNull Long transitionId) {
			this(transitionId, null);
		}

		public TransitionNotAllowedException(@NonNull Long transitionId, @Nullable Throwable cause) {
			super("Transition with id " + transitionId + " not allowed for current state!", cause);
		}

	}

}
