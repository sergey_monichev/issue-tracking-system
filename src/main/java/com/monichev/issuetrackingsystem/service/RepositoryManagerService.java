package com.monichev.issuetrackingsystem.service;

/**
 * A service interface to operate repository data.
 */
public interface RepositoryManagerService {

	/**
	 * Fill repository with initial data.
	 */
	void init();

	/**
	 * Cleanup repository data.
	 */
	void cleanup();

}
