# Issue Tracking System

## Build
```
mvnw clean package
```

## Run
```
mvnw spring-boot:run
```

## Business Entities

### Issue
Represents issue that has a state and possible transitions according to its state.
Issue associated with workflow.

### Workflow
Workflow encapsulates states and transitions.

### State
Represents issue state.
In case we remove state all associated transitions are also will be removed.

### Transition
Represents a transition between states.
Transition could have an action.
Transitions always have associated From and To states.

#### Initial transition
There is a special case of transitions - initial one.
It is unique per workflow and performs the very first transition when an issue created.
Initial transition can't be deleted, it has from state as null.

## Demo data
After the first start database schema will be created with no data in it.
To init demo data http://localhost:8080/v1/repositoryManager/init endpoint should be called.
To clean up all the data http://localhost:8080/v1/repositoryManager/cleanup endpoint should be called.

Demo data contains a demo workflow without issues.

### Demo workflow
![Demo workflow](./doc/demo-workflow.png)

### Using scenarios

#### Working with issue (/v1/issue)
 1. create issue (issue automatically associates with default demo workflow)
 2. get issue transitions
 3. do transitions
 4. delete/update issue id needed

#### Working with states (/v1/workflow)
 1. get workflow
 2. create/read/update/delete states

#### Working with transitions (/v1/workflow)
 1. get workflow
 2. create/read/update/delete transitions

#### Working with demo data (/v1/repositoryManager)
 1. init data
 2. clean up data

More specific using scenarios with API calls could be taken
from the test cases in the `src/test/resources/features` directory.

## API documentation
http://localhost:8080/swagger-ui.html

## TODO
### Add security layer
```
1. Add spring security support
2. Add User entity with fields:
  * username - unique username among system 
  * password - bcrypted password
  * authority - granted roles
  * enabled - active flag
3. Add Roles "ROLE_ADMIN", "ROLE_USER".
4. Assign @Secured annotation where it's required according Roles.
5. Add fields to Entity:
  * creator - User who created the issue
  * assigned - User who assigned to the issue
6. Add logic to manipulate Users 
```

### Add user support
```
1. Implement "Add security layer" TODO
2. Add Person entity with fields:
  * firstName - first name
  * lastName - last name
  * middleName - middle name
  * user - associated User account
3. Add logic to manipulate Persons
```